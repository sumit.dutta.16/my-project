const audiBMW = (inventory) =>{
    let res = [];
    for(i=0; i< inventory.length; i++){
        if(inventory[i].car_make === "Audi" || inventory[i].car_make === "BMW"){
            res.push(inventory[i]);
        }
    }
    return res;
}

module.exports = { audiBMW };