const sortedCarModel = (inventory) => {
    return inventory.sort((a, b) => {

        const modelA = a.car_model.toUpperCase();
        const modelB = b.car_model.toUpperCase();
  
        let comparison = 0;
        if(modelA > modelB){
            comparison = 1;
        }
        else if(modelA < modelB){
            comparison = -1;
        }

        return comparison;
    });
};
module.exports = { sortedCarModel };